const express = require('express');
const app = express();

const NAME = process.env.NAME;


app.get('/', (req, res) => {
  res.status(200).send(`Hello ${NAME}`);
});

app.listen(8080, () => console.log('listening on port 8080'));